package edu.energyaccounting.timewidget;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.InputEvent;
import javafx.util.StringConverter;


import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.logging.Logger;


public class TimeSpinner extends Spinner<LocalTime> {
    final static Logger lOG = Logger.getLogger("TimeSpinnerWidget");

    private final ObjectProperty<Mode> mode = new SimpleObjectProperty<>(Mode.HOURS);

    public TimeSpinner(LocalTime time) {
        setEditable(true);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        StringConverter<LocalTime> localTimeStringConverter = new StringConverter<LocalTime>() {
            @Override
            public String toString(LocalTime time) {
                return formatter.format(time);
            }

            @Override
            public LocalTime fromString(String string) {
                String[] box = string.split(":");
                int hours = getIntField(box, 0);
                int minutes = getIntField(box, 1);
                int seconds = getIntField(box, 2);
                int totalSecond = (hours * 60 + minutes) * 60 + seconds;
                int totalMinutes = (hours * 60 + minutes);
//               return LocalTime.of((totalSecond/3600)%24, (totalSecond/60)%60,seconds%60);
                return LocalTime.of((totalMinutes / 60) % 24, minutes % 60);
            }

            private int getIntField(String[] tokens, int index) {
                if (tokens.length <= index || tokens[index].isEmpty()) {
                    return 0;
                }
                return Integer.parseInt(tokens[index]);
            }
        };
        TextFormatter<LocalTime> textFormatter = new TextFormatter<>(localTimeStringConverter, time, t -> {
            String newText = t.getControlNewText();
            if (newText.matches("[0-9]{0,2}:[0-9]{0,2}")) {
                return t;
            }
            return null;
        });

        SpinnerValueFactory<LocalTime> valueFactory = new SpinnerValueFactory<LocalTime>() {
            {
                setConverter(localTimeStringConverter);
                setValue(time);
            }

            @Override
            public void decrement(int steps) {
                setValue(mode.get().decrement(getValue(), steps));
                mode.get().select(TimeSpinner.this);
            }

            @Override
            public void increment(int steps) {
                setValue(mode.get().increment(getValue(), steps));
                mode.get().select(TimeSpinner.this);
            }
        };
        this.setValueFactory(valueFactory);
        this.getEditor().setTextFormatter(textFormatter);

        this.getEditor().addEventFilter(InputEvent.ANY, event -> {
            int caretPos = this.getEditor().getCaretPosition();
            int hrIndex = this.getEditor().getText().indexOf(':');

            if (caretPos <= hrIndex) {
                mode.set(Mode.HOURS);
            } else {
                mode.set(Mode.MINUTES);

            }

        });

        /*
         * автоматом меняет значение объекта, при редактировании через TextField
         */
        this.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            Logger LOG = Logger.getLogger("TimeSpinner");
            try {
                LocalTime tmp = LocalTime.parse(newValue);
                TimeSpinner.this.getValueFactory().setValue(tmp);
            }catch (DateTimeParseException e){
                LOG.warning("ошибка парсинга времени");
            }

        });
        mode.addListener(((observable, oldMode, newMode) -> newMode.select(this)));

    }

    public TimeSpinner() {
        this(LocalTime.of(8,0));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TimeSpinner.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObjectProperty<Mode> modeProperty() {

        return mode;
    }

    public final Mode getMode() {
        return modeProperty().get();
    }

    public final void setMode(Mode mode) {
        modeProperty().set(mode);
    }

    enum Mode {

        HOURS {
            @Override
            LocalTime increment(LocalTime time, int steps) {
                return time.plusHours(steps);
            }

            @Override
            void select(TimeSpinner timeSpinner) {
                int index = timeSpinner.getEditor().getText().indexOf(':');
                timeSpinner.getEditor().selectRange(0, index);
            }
        },
        MINUTES {
            @Override
            LocalTime increment(LocalTime time, int steps) {
                return time.plusMinutes(steps);
            }

            @Override
            void select(TimeSpinner timeSpinner) {
                int index = timeSpinner.getEditor().getText().lastIndexOf(':');
                timeSpinner.getEditor().selectRange(index + 1, timeSpinner.getEditor().getText().length());
            }
        };

        abstract LocalTime increment(LocalTime time, int steps);

        abstract void select(TimeSpinner timeSpinner);

        LocalTime decrement(LocalTime time, int steps) {
            return increment(time, -steps);
        }
    }

}

